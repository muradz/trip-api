package com.tripapi.service;

import com.tripapi.domain.Company;
import org.springframework.stereotype.Service;


public interface CompanyService {
    Company register(Company company);
}
