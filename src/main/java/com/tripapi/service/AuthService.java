package com.tripapi.service;

public interface AuthService {
    Object login(String username, String password);
}
