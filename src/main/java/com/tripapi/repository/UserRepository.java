package com.tripapi.repository;

import com.tripapi.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Murad Zulfugarov
 */

public interface UserRepository extends CrudRepository<User, Long> {
    User findByPhone(String phone);
}
