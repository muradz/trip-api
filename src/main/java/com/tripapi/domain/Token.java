package com.tripapi.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

/**
 * @author Murad Zulfugarov
 */

@Data
public class Token {
    @JsonAlias("access_token")
    private String accessToken;

    @JsonAlias("token_type")
    private String tokenType;

    @JsonAlias("jti")
    private String jti;

    @JsonAlias("expires_in")
    private Long expiresIn;

    private String scope;
}
