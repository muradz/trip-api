package com.tripapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Murad Zulfugarov
 */

@Data
@AllArgsConstructor
public class ErrorResponse {
    private String message;
    private int status;
}