package com.tripapi.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripapi.domain.Company;
import com.tripapi.domain.Token;
import com.tripapi.domain.User;
import com.tripapi.exception.UnauthorizedUserException;
import com.tripapi.exception.UserAlreadyExistsException;
import com.tripapi.repository.CompanyRepository;
import com.tripapi.repository.UserRepository;
import com.tripapi.service.AuthService;
import com.tripapi.util.Constant;
import com.tripapi.util.Message;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AuthServiceImpl implements AuthService, UserDetailsService {
    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    @Value("${triper.auth.url}")
    private String authUrl;

    @Value("${security.jwt.client-id}")
    private String clienId;

    @Value("${security.jwt.client-secret}")
    private String clientSecret;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Object login(String username, String password) {

        String credentials = clienId + ":" + clientSecret;
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        int statusCode = 0;
        try {
            HttpClient client = new HttpClient();
            PostMethod method = new PostMethod(authUrl);

            method.setRequestHeader(Constant.AUTHORIZATION, "Basic " + encodedCredentials);
            method.setParameter(Constant.GRANT_TYPE, "password");
            method.setParameter(Constant.USERNAME, username);
            method.setParameter(Constant.PASSWORD, password);

            client.executeMethod(method);

            statusCode = method.getStatusCode();
            if (statusCode == 200) {
                String response = method.getResponseBodyAsString();
                ObjectMapper mapper = new ObjectMapper();
                Token token = mapper.readValue(response, Token.class);

                Company company = companyRepository.findByPhone(username);

                if (company == null) {
                    User user = userRepository.findByPhone(username);
                    user.setToken(token);
                    return user;
                } else {
                    logger.info("User logged in: {}" + company.toString());
                    company.setToken(token);
                    return company;
                }

            } else {
                throw new UnauthorizedUserException(Message.UNAUTHORIZED);
            }
        } catch (IOException e) {
            logger.error("Error occurred while login with status code :{}. " +
                    "Username: {}", statusCode, username);
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        UserDetails userDetails;
        List<GrantedAuthority> authorities = new ArrayList<>();

        User user = userRepository.findByPhone(s);
        if (user != null) {
            user.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
            });

            userDetails = new org.springframework.security.core.userdetails.
                    User(user.getPhone(), user.getPassword(), authorities);
        } else {
            Company company = companyRepository.findByPhone(s);

            company.getRoles().forEach(role -> {
                authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
            });

            userDetails = new org.springframework.security.core.userdetails.
                    User(company.getPhone(), company.getPassword(), authorities);
        }

        if (userDetails == null)
            throw new UsernameNotFoundException(String.format("The username doesn't exist", s)); //TODO change it (Murad)

        return userDetails;
    }
}
